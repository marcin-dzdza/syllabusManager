// Module syllabus

$(document).ready(function(){
    syllabus.init();
});

var syllabus = (function(){
    //private scope
    var expandTogglePrototype = $(
        '<span class="expand-toggle glyphicon glyphicon-folder-open"></span>'
    );
    var editTogglePrototype = $(
        '<span class="edit-toggle glyphicon glyphicon-edit">'
        + '</span>'
        );
    var removeBtnPrototype = $(
        '<span class="remove-btn glyphicon glyphicon-remove">'
        + '</span>'
        );
    var addBtnPrototype = $(
        '<span class="add-btn glyphicon glyphicon-plus">'
        + '</span>'
        );
    var editFormPrototype = $(
        '<div class="edit-form">'
        + '<form action="#">'
        + '<div class="form-group">'
        + '<label for="edit">Podaj nową wartość: </label>'
        + '<textarea class="edit-area form-control" id="edit">'
        + '</textarea>'
        + '</div>'
        + '<button class="edit-submit btn btn-default">OK</button>'
        + '<button class="edit-cancel btn btn-default">Cancel</button>'
        + '</form>'
        + '</div>'
    );
    var messageBoxPrototype = $(
        '<div class="edit-message alert alert-success">'
        + 'Zmiana została zapisana'
        + '</div>'
    );
    var allExpandToggles;
    var allEditToggles;
    var allAddBtns;
    var allRemoveBtns;
    var currentForm;
    var currentTextArea;
    var currentSubmitBtn;
    var currentCancelBtn;
    var editableField;
    var editableFieldBox;
    var fieldId;
    var currentMessageBox;
    var messageHideTimer;
    var currentList;
    var newItemParent;
    var areCommentsVisible = true;
    var areDescriptionsVisible = true;
    var areTopicContentsVisible = false;
        
    var addExpandToggles = function() {
        $('.expandable-title').append(expandTogglePrototype.clone());
        allExpandToggles = $('.expand-toggle');
    };
    
    var addEditToggles = function() {
        $('.editable-box').append(editTogglePrototype.clone());
        allEditToggles = $('.edit-toggle');        
    };
    
    var addRemoveBtns = function() {
        $('.expandable-title').append(removeBtnPrototype.clone());
        allRemoveBtns = $('.remove-btn');        
    };
    
    var addAddBtns = function() {
        $('.expandable-title:not(.Task)').append(addBtnPrototype.clone());
        allAddBtns = $('.add-btn');        
    };

    var removeAllButtons = function() {
        allExpandToggles.remove();
        allEditToggles.remove();
        allAddBtns.remove();
        allRemoveBtns.remove();
    };
    
    var removeAllEditForms = function() {
        $('.edit-form').remove();
    };
    
    var removeAllMessages = function() {
        $('.edit-message').remove();
    };
    
    var expandToggleAction = function() {
        removeAllEditForms();
        removeAllMessages();
        $(this).parent('.expandable-title').next('.expandable-content').toggle();       
    };
    
    var createMessage = function(classes, msg) {
        var messageBox = $('<div>');
        $(messageBox).text(msg);
        $(messageBox).addClass(classes);
        return messageBox;        
    };
    
    var ajaxSaveEdit = function() {
        console.log('początek działania');    
        $.ajax({

            url : '../edit',
            cache: false,
            type : 'GET',
            data : {
                'fieldId' : fieldId,
                'newValue' : currentTextArea.val()
            },
            dataType:'html',
            success : function(data) {            
                if (data == '(server) Changes saved') {
                    console.log('Serwer przesłał info: changes saved');
                    editableField.text(currentTextArea.val());
                    removeAllMessages();
                    currentForm.before(createMessage('edit-message alert alert-success', data));
                    currentMessageBox = currentForm.prev();
                    messageHideTimer = setTimeout(function(){
                        currentMessageBox.remove();
                    }, 2000);
                    currentForm.remove();
                } else {
                    console.log('Serwer przesłał inne info:' + data);
                    removeAllMessages();
                    currentForm.prepend(createMessage('edit-message alert alert-danger', 'Failed to save changes for unknown reasons'));
                }

            },
            error : function(request,error)
            {
                removeAllMessages();
                currentForm.prepend(createMessage('edit-message alert alert-danger', 'Failed to save changes (server error)'));
            }
        });        
    };
    
    var editToggleAction = function() {
        var submitBtnAction = function(event) {
            event.preventDefault();
            removeAllMessages();
            currentForm.prepend(createMessage('edit-message alert alert-info', 'Zapisywanie zmian w toku...'));
            ajaxSaveEdit();
        };
        var cancelBtnAction = function(event) {
            event.preventDefault();
            currentForm.remove();
        };
        
        removeAllEditForms();
        removeAllMessages();
        $(this).parent().after(editFormPrototype.clone());
        currentForm = $(this).parent().next('.edit-form');
        currentTextArea = currentForm.find('textarea');
        currentSubmitBtn = currentForm.find('button.edit-submit');
        currentCancelBtn = currentForm.find('button.edit-cancel');
        editableField = $(this).parent().find('.editable-text');
        editableFieldBox = $(this).parent();
        fieldId = editableFieldBox.attr('id');
        currentTextArea.val(editableField.text());
        currentTextArea.focus();
        currentSubmitBtn.click(submitBtnAction);
        currentCancelBtn.click(cancelBtnAction);        
    };
   
    var ajaxAdd = function() {
        console.log('początek działania');    
        $.ajax({
            url : '../add',
            cache: false,
            type : 'GET',
            data : {
                'fieldId' : fieldId,
            },
            dataType:'html',
            success : function(data) {            
                if (data == '(server)Failed to add new item') {
                    console.log('Serwer przesłał inne info:' + data);
                    removeAllMessages();
                    currentList.prepend(createMessage('edit-message alert alert-danger', 'Failed to add new item for unknown reasons'));
                } else {
                    console.log('Serwer przesłał info: changes saved');
                    currentList.append(data);
                    removeAllButtons();
                    init();
//                    editableField.text(currentTextArea.val());
//                    removeAllMessages();
//                    currentForm.before(createMessage('edit-message alert alert-success', data));
//                    currentMessageBox = currentForm.prev();
//                    messageHideTimer = setTimeout(function(){
//                        currentMessageBox.remove();
//                    }, 2000);
//                    currentForm.remove();
                }
            },
            error : function(request,error)
            {
                removeAllMessages();
                currentList.prepend(createMessage('edit-message alert alert-danger', 'Failed to add new item (server error)'));
            }
        });        
    };
    
    var addBtnAction = function() {
        event.preventDefault();
        removeAllEditForms();
        removeAllMessages();
        currentList = $(this)
            .parent('.expandable-title')
            .next('.expandable-content')
            .find('.item-list')
            .first();
        newItemParent = $(this).parent('.expandable-title');
        console.log(currentList);
        console.log(newItemParent);
        fieldId = newItemParent.attr('id');
        ajaxAdd(); 
    };

    var allTopicContentsToggleAction = function() {
        if (areTopicContentsVisible == true) {
            $('.Topic.content').hide();
            areTopicContentsVisible = false;
        } else {
            $('.Topic.content')
                .parent('.item-list:visible')
                .find('.Topic.content')
                .show();
            areTopicContentsVisible = true;
        }
    };    

    var commentsToggleAction = function() {
        if (areCommentsVisible == true) {
            $('.user-syllabus').hide();
            areCommentsVisible = false;
        } else {
            $('.user-syllabus')
                .parent('.content:visible')
                .find('.user-syllabus')
                .show();
            areCommentsVisible = true;
        }
    };

    var descriptionsToggleAction = function() {
        if (areDescriptionsVisible == true) {
            $('.description').hide();
            areDescriptionsVisible = false;
        } else {
            $('.description')
                .parent('.content:visible')
                .find('.description')
                .show();
            areDescriptionsVisible = true;
        }
    };
    
    var bindGlobalBtns = function() {
        var commentsToggle = $('#comments-toggle');
        var descriptionsToggle = $('#descriptions-toggle');
        var allTopicContentsToggle = $('#all-topic-content-toggle');
        commentsToggle.click(commentsToggleAction);
        descriptionsToggle.click(descriptionsToggleAction);
        allTopicContentsToggle.click(allTopicContentsToggleAction);
    };
    
    var bindUI = function() {
        allExpandToggles.click(expandToggleAction);
        allEditToggles.click(editToggleAction);
        allAddBtns.click(addBtnAction);
        bindGlobalBtns();
    };     
    
    var init = function() {

        addEditToggles();
        addRemoveBtns();
        addExpandToggles();
        addAddBtns();
        bindUI();
    };

    //public scope
    return {
        settings: {
            setting1: 1,
            setting2: 2
        },
        init: init
    }
})();