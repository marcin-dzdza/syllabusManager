Struktura klas w mysyllabus.phtml

ROZWIJALNOŚĆ
- każdy element, który może się schować, ma klasę: expandable-content. Jest to wszystko poza tytułem danego obiektu
- bezpośrednio nad nim musi być element o klasie expandable-title  - tytuł danego obiektu
- skrypt dodaje na koniec dzieci expandable-title przycisk o klasie expand-toggle, który ukrywa i pokazuje expandable-content
- aby wszystko działało ok, expandable-title powinien zawierać element inline (np. span) z tekstem do wyświetlenia. Wtedy przycisk zostanie umieszczony obok niego

EDYTOWALNOŚĆ
- każdy element, który można edytować, ma klasę editable-text i musi być inline, np. span. W takim elemencie nie może być nic poza edytowalnym tekstem
- aby dodać do edytowalnego tekstu jakąś etykietę, umieść ją w oddzielnym spanie
- element ten musi znajdować się wewnątrz elementu o klasie editable-box
- skrypt dodaje na koniec dzieci editable-box przycisk o klasie edit-toggle, który tworzy formularz edycji

OZNACZENIA WYJAŚNIAJĄCE STRUKTURĘ
- wszystkie poniższe szczegółowe klasy i id dotyczą diva o klasie "editable-box", a nie "editable-text"
- każdy element ma id wg wzoru: table.id.column, np. category.12.title
- div będący expandable-title ma klasę: syllabus-title, category-title itd., a będący expandable-content: syllabus-content
- każdy div wewnątrz ...-content ma klasę z nazwą parametru, np. category-description, category-comment
- jedną z powyższych klas jest klasa item-list, zawierająca dzieci. Ma dodatkową kasę: syllabus-categories, category-topics 
- każdy element związany z użytkownikiem, a nie sylabusem, ma dodatkową klasę "user-syllabus"


FORMULARZ, WIADOMOŚĆ
- wszystko widać w prototypie, nie trzeba tłumaczyć