<?php
namespace Syllabus\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Syllabus\Model\SyllabusItemRepositoryInterface;
use Syllabus\Model\UserRepositoryInterface;
use Syllabus\Model\SyllabusItem;

//tylko na potrzeby filtra i może trzeba zmienić
use Syllabus\Model\Syllabus;

//usuń jeśli zrezygnujesz z sesji
use Zend\Session\Container;

/**
 * @author Marcin Dżdża
 * @version 1.0
 */
class SyllabusController extends AbstractActionController
{
    /**
     *
     * @var SyllabusItemRepositoryInterface For getting syllabus data from DB
     */
    private $syllabusRepository;
    /**
     *
     * @var UserRepositoryInterface Gets data from 'users' in DB
     */
    private $userRepository;

    /**
     * @param SyllabusItemRepositoryInterface $syllabusRepository
     * @param UserRepositoryInterface $userRepository
     * @return void
     */
    public function __construct(
        SyllabusItemRepositoryInterface $syllabusRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->syllabusRepository = $syllabusRepository;
        $this->userRepository = $userRepository;
    }
    
    /**
     * displaying one syllabus without user-specific info
     * @return ViewModel
     */
    public function indexAction()
    {
        $syllabusId = $this->params()->fromRoute('id');
        if (!empty($syllabusId) && is_numeric($syllabusId)
            && intval($syllabusId) == $syllabusId && $syllabusId > 0) {
            $syllabus = $this->syllabusRepository->findItemById($syllabusId);
        }
        
        if (isset($syllabus)) {
            $syllabus->addChildren(
                $this->syllabusRepository
                    ->getRepositoryByClass('Syllabus\Model\Category')
                    ->findItemsByParentId($syllabus->getId())
                );
            foreach ($syllabus->getAllChildren() as $category) {
                $category->addChildren(
                        $this->syllabusRepository
                            ->getRepositoryByClass('Syllabus\Model\Topic')
                            ->findItemsByParentId($category->getId())
                    );
                foreach ($category->getAllChildren() as $topic) {
                    $topic->addChildren(
                            $this->syllabusRepository
                                ->getRepositoryByClass('Syllabus\Model\Task')
                                ->findItemsByParentId($topic->getId())
                    );
                }
            }
            return new ViewModel([
                'syllabus' => $syllabus
            ]);
        
        }
    }

    /**
     * displaying one syllabus with user-specific info
     * @return ViewModel
     * @todo Tymczasowo w sesji zawsze będzie ten sam użytkownik nr 1. Po dodaniu modułu User trzeba będzie zmienić
     */
    public function mysyllabusAction()
    {
        $sessionUser = new Container('user');
        $sessionUser->id = 1;
        $user = $this->userRepository->findUserById($sessionUser->id);
        $this->syllabusRepository->setUser($user);
        
        $syllabusId = $this->params()->fromRoute('id');
        if (!empty($syllabusId) && is_numeric($syllabusId)
            && intval($syllabusId) == $syllabusId && $syllabusId > 0) {
            $syllabus = $this->syllabusRepository->findItemById($syllabusId);
        }
        
        if (isset($syllabus)) {
            $syllabus->addChildren(
                $this->syllabusRepository
                    ->getRepositoryByClass('Category')
                    ->findItemsByParentId($syllabus->getId())
                );
            foreach ($syllabus->getAllChildren() as $category) {
                $category->addChildren(
                        $this->syllabusRepository
                            ->getRepositoryByClass('Topic')
                            ->findItemsByParentId($category->getId())
                    );
                foreach ($category->getAllChildren() as $topic) {
                    $topic->addChildren(
                            $this->syllabusRepository
                                ->getRepositoryByClass('Task')
                                ->findItemsByParentId($topic->getId())
                    );
                }
            }
            
            return new ViewModel([
                'syllabus' => $syllabus
            ]);
        
        }
    }
    
    /**
     * Called by editAction to initially preprare data sent by ajax request to edit
     */
    public function getAjaxEditValues()
    {
        /* get data from ajax request */       
        $fieldId = $this->getRequest()->getQuery('fieldId',null);
        $newValue = $this->getRequest()->getQuery('newValue',null);
        $fieldId = explode('.', $fieldId); // null też zmieni się w tablicę
        if (count($fieldId) == 3) {
            $input = array_combine(['table', 'id', 'column'], $fieldId);

            /*tutaj ogólny filtr dotyczący table i id, zawarty w syllabus*/
            $inputFilter = SyllabusItem::getGeneralEditFilter();
            $inputFilter->setData($input);

            /*jeśli dane przeszły pierwszy filtr: */
            if ($inputFilter->isValid()) {
                return [
                    'table' => $inputFilter->getValues()['table'],
                    'id' => $inputFilter->getValues()['id'],
                    'column' => $inputFilter->getValues()['column'],
                    'newValue' => $newValue
                ];
            }
        }
        return null;
    }
    
    public function editAction()
    {
        /* If request does not come from ajax, send 404 */
        /*if (!($this->getRequest()->isXmlHttpRequest())) {
            $this->getResponse()->setStatusCode(404);
            return false;
        }
        
        /* Get data from session */
        $sessionUser = new Container('user');
        if (isset($sessionUser->id)) {
            $this->syllabusRepository->setUser(
                $this->userRepository->findUserById($sessionUser->id)
            );
        }
        
        /* message in case of failure */ 
        $resultMessage = 'Failed to save changes';
        
        $ajaxValues = $this->getAjaxEditValues();
        if (isset($ajaxValues)) {
            /* Creating object based on given data value */
            $item = $this->syllabusRepository
                ->getRepositoryByClass($ajaxValues['table'])
                ->findItemById($ajaxValues['id']);
            if (isset ($item)) {

                $input = [
                    'id' => $ajaxValues['id'],
                    $ajaxValues['column'] => $ajaxValues['newValue']
                ];
                $inputFilter = $item->getInputFilter();
                $inputFilter->setData($input);
                if ($inputFilter->isValid()) {
                    $item->exchangeArray($inputFilter->getValues());
                    try {
                        $this->syllabusRepository
                            ->getRepositoryByClass($ajaxValues['table'])
                            ->updateItem($item);
                        $resultMessage = 'Changes saved';
                    } catch (Exception $ex) {

                    }
                }
            }
        }
        /* Creating custom ViewModel to send ajax response without layout */
        $customViewModel = new ViewModel(['resultMessage' => $resultMessage]);
        $customViewModel->setTerminal(true);
        return $customViewModel;
    }
    
    /**
     * Called by addAction to initially preprare data sent by ajax request to add
     */
    public function getAjaxAddValues()
    {
        /* get data from ajax request */       
        $fieldId = $this->getRequest()->getQuery('fieldId',null);
        $fieldId = explode('.', $fieldId); // null też zmieni się w tablicę
        if (count($fieldId) == 3) {
            $input = array_combine(['table', 'id', 'column'], $fieldId);

            /*tutaj ogólny filtr dotyczący table i id, zawarty w syllabus*/
            $inputFilter = SyllabusItem::getGeneralEditFilter();
            $inputFilter->setData($input);

            /*jeśli dane przeszły pierwszy filtr: */
            if ($inputFilter->isValid()) {
                return [
                    'table' => $inputFilter->getValues()['table'],
                    'id' => $inputFilter->getValues()['id'],
                    'column' => $inputFilter->getValues()['column'],
                ];
            }
        }
        return null;
    }
    
    public function addAction()
    {
        /* If request does not come from ajax, send 404 */
        /*if (!($this->getRequest()->isXmlHttpRequest())) {
            $this->getResponse()->setStatusCode(404);
            return false;
        }
        
        /* Get data from session */
        $sessionUser = new Container('user');
        if (isset($sessionUser->id)) {
            $this->syllabusRepository->setUser(
                $this->userRepository->findUserById($sessionUser->id)
            );
        }
        
        /* message in case of failure */ 
        $resultMessage = 'Failed to add new item';
        
        $ajaxValues = $this->getAjaxAddValues();
        //$ajaxValues = ['parentTable' => 'Syllabus', 'parentId' => 1];
        if (isset($ajaxValues)) {
            $currentRepository = $this->syllabusRepository
                ->getRepositoryByClass($ajaxValues['table']);
            $currentItem = $currentRepository->findItemById($ajaxValues['id']);
            while ($currentRepository = $currentRepository->getDirectChild()) {
                $newItem = $currentRepository->createItem();
                $newItem->exchangeArray([
                    'title' => 'New element',
                    'description' => 'No description',
                    'comments' => 'No comment'
                ]);
                try {
                    $newItem = $currentRepository->insertItem($newItem, $currentItem);
                    $resultMessage = 'Item added'; //niedoskonałe
                } catch (Exception $ex) {

                }
                if (!isset($topLevelItem)) {
                    $topLevelItem = $newItem; //top level in view.phtml
                    $currentItem = $newItem;
                } else {
                    $currentItem->addChild($newItem);
                    $currentItem = $newItem;
                }
            }
            
        }
        /* Creating custom ViewModel to send ajax response without layout */
        $customViewModel = new ViewModel([
            'resultMessage' => $resultMessage,
            'topLevelItem' => (isset($topLevelItem)) ? $topLevelItem : null
        ]);
        $customViewModel->setTerminal(true);
        return $customViewModel;
        
        //aby sprawnie działało, akcje dodawania przycisków muszą być ponownie wykonane po każdym dodaniu, więc najpierw powinny usunąć wszystkie przyciski, a potem dodać ponownie

    }
    
}