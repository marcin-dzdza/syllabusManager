<?php
namespace Syllabus\Factory;

use Interop\Container\ContainerInterface;
use Syllabus\Model\Syllabus;
use Syllabus\Model\Category;
use Syllabus\Model\Topic;
use Syllabus\Model\Task;
use Syllabus\Model\SyllabusRepository;
use Syllabus\Model\CategoryRepository;
use Syllabus\Model\TopicRepository;
use Syllabus\Model\TaskRepository;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\ArraySerializable;
use Zend\ServiceManager\Factory\FactoryInterface;

class SyllabusRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $syllabusRepository = new SyllabusRepository(
            $container->get(AdapterInterface::class),
            new ArraySerializable(),
            new Syllabus('', '', -1, null, -1)
        );
        
        $categoryRepository = new CategoryRepository(
            $container->get(AdapterInterface::class),
            new ArraySerializable(),
            new Category('', '', -1, null)
        );
        
        $topicRepository = new TopicRepository(
            $container->get(AdapterInterface::class),
            new ArraySerializable(),
            new Topic('', '', -1, null, 0)
        );
        
        $taskRepository =  new TaskRepository(
            $container->get(AdapterInterface::class),
            new ArraySerializable(),
            new Task('', '', -1, null)
        );
        
        $syllabusRepository->setChild($categoryRepository);
        $categoryRepository->setChild($topicRepository);
        $topicRepository->setChild($taskRepository);
        
        return $syllabusRepository;
    }
}