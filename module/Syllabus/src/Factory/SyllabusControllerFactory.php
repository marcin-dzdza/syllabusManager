<?php
namespace Syllabus\Factory;

use Syllabus\Controller\SyllabusController;
use Syllabus\Model\SyllabusItemRepositoryInterface;
use Syllabus\Model\UserRepositoryInterface;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SyllabusControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SyllabusController(
            $container->get(SyllabusItemRepositoryInterface::class),
            $container->get(UserRepositoryInterface::class)
        );
    }
}