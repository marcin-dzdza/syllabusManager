<?php
namespace Syllabus\Factory;

use Interop\Container\ContainerInterface;
use Syllabus\Model\User;
use Syllabus\Model\SqlUserRepository;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\ServiceManager\Factory\FactoryInterface;

class SqlUserRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SqlUserRepository(
            $container->get(AdapterInterface::class),
            new ReflectionHydrator(),
            new User('', '', '', '', 0, 0, -1)
        );
    }
}