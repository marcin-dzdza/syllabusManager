<?php
namespace Syllabus\Model;

use Zend\Db\ResultSet\ResultSetInterface;

class User
{
    private $id;
    private $email;
    private $password;
    private $password_salt;
    private $name;
    private $status;
    private $creation_date;

    public function __construct($email, $password, $password_salt, $name, $status, $creationDate, $id = null)
    {
        $this->email = $email;
        $this->password = $password;
        $this->password_salt = $password_salt;
        $this->name = $name;
        $this->status = $status;
        $this->creationDate = $creationDate;
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getPassword()
    {
        return $this->password;
    }

    function getPassword_salt()
    {
        return $this->password_salt;
    }

    function getName()
    {
        return $this->name;
    }

    function getStatus()
    {
        return $this->status;
    }

    function getCreation_date()
    {
        return $this->creation_date;
    }


}