<?php
namespace Syllabus\Model;

use Syllabus\Model\SyllabusItemRepository;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\HydratorInterface;

/**
 * For communication between class Syllabus and DB
 */
class SyllabusRepository extends SyllabusItemRepository
{
    /**
     * @var Syllabus
     */
    protected $objectPrototype;
    
    public function __construct(
        AdapterInterface $adapter,
        HydratorInterface $hydrator,
        Syllabus $objectPrototype
    )
    {
        parent::__construct($adapter, $hydrator, $objectPrototype);
    }

    /**
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findAllItems()
    {
        
    }

    /**
     * @param int $id Id of the item
     * @return SyllabusItemInterface
     */
    public function findItemById($id)
    {
        return $this->universalFindItemById(
            $id,
            'syllabuses',
            'users_syllabuses',
            'syllabus_id',
            [
                'comments'
            ],
            'syllabus'
        );
    }
    
    /**
     * @param int $id Id of the parent item
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findItemsByParentId($id)
    {

    }
    
    /**
     * @param SyllabusItemInterface $item
     * @param SyllabusItemInterface $parent If no parent, give $item twice
     */
    public function insertItem(SyllabusItemInterface $item, SyllabusItemInterface $parent)
    {     
        $newId = $this->universalInsertItem(
            'syllabuses',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId(),
                'access' => $item->getAccess()
            ],
            'syllabus',
            'users_syllabuses',
            [
                'user_id' => $item->getUserId(),
                'comments' => $item->getUserComments()
            ],
            'syllabus_id',
            '',
            [
            ]
        );
        
        return new Syllabus(
            $item->getTitle(),
            $item->getDescription(),
            $item->getUserId(),
            $item->getUserComments(),
            $item->getAccess(),
            $newId
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function updateItem(SyllabusItemInterface $item)
    {        
        return $this->universalUpdateItem(
            $item,
            'syllabuses',
            'users_syllabuses',
            'syllabus_id',
            'syllabus',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId(),
                'access' => $item->getAccess()
            ],
            [
                'comments' => $item->getUserComments()
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function deleteItem(SyllabusItemInterface $item)
    {
        
    }
    
}