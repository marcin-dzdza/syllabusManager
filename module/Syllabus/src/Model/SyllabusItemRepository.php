<?php
namespace Syllabus\Model;

use Syllabus\Model\SyllabusItemRepositoryInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\HydratorInterface;

use InvalidArgumentException;
use RuntimeException;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Insert;

abstract class SyllabusItemRepository implements SyllabusItemRepositoryInterface
{
    /**
     * @var AdapterInterface Database connection 
     */
    protected $adapter;
    /**
     * @var HydratorInterface For transforming DB columns into object properties
     */
    protected $hydrator;
    /**
     * @var SyllabusItemInterface
     */
    protected $objectPrototype;
    /**
     * If provided by setUser, changes the behaviour of all methods to deal with the fact that data is also related to db table 'users_*'
     * @var User|null 
     */
    protected $user;
    /**
     * @var SyllabusItemRepositoryInterface|null 
     */
    protected $childRepository;
    
    public function __construct(
        AdapterInterface $adapter,
        HydratorInterface $hydrator,
        SyllabusItemInterface $objectPrototype
    ) {
        $this->adapter = $adapter;
        $this->hydrator = $hydrator;
        $this->objectPrototype = $objectPrototype;
    }

    /**
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    abstract public function findAllItems();
    
    /**
     * Method called by findItemById. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters
     * @param int $id
     * @param string $itemsTableName e.g. "categories"
     * @param string $usersItemsTableName e.g. "users_categories"
     * @param string $usersJoinColumnName e.g. "category_id"
     * @param string[] $usersItemsColumnArray e.g. ["comments"]
     * @param string $objectClassName e.g. "category"
     * @return SyllabusItemInterface
     */
    protected function universalFindItemById($id, $itemsTableName, $usersItemsTableName, $usersJoinColumnName, $usersItemsColumnArray, $objectClassName)
    {  
        $sql = new Sql ($this->adapter);
        $select = $sql->select();
        $select->from($itemsTableName);
        $whereArray = [$itemsTableName . ".id = $id"];

        if (isset($this->user)) {
            $userId = $this->user->getId();
            $select->join(
                $usersItemsTableName,
                "$itemsTableName.id = $usersItemsTableName.$usersJoinColumnName",
                $usersItemsColumnArray,
                $select::JOIN_LEFT
            );
            $whereArray[] = "($usersItemsTableName.user_id = $userId OR $usersItemsTableName.user_id IS NULL)";
        }
        $select->where($whereArray);
        $select->limit(1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();      
        if (! $result instanceof ResultInterface || ! $result->isQueryResult()) {
            throw new RuntimeException("Failed retrieving $objectClassName with identifier $id; unknown database error.");
        }
        $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
        $resultSet->initialize($result);
        $singleObject = $resultSet->current();

        if (! $singleObject) {
            throw new InvalidArgumentException("$objectClassName with identifier $id not found.");
        }

        return $singleObject;
    }

    /**
     * @param int $id Id of the item
     * @return SyllabusItemInterface
     */
    abstract public function findItemById($id);

    /**
     * Method called by findItemsByParentId. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters
     * @param int $id
     * @param string $itemsTableName e.g. "categories"
     * @param string $parentChildTableName e.g. "syllabuses_categories"
     * @param string $childColumnName e.g. "category_id"
     * @param string $parentColumnName e.g. "syllabus_id"
     * @param string $usersItemsTableName e.g. "users_categories"
     * @param string $usersJoinColumnName e.g. "category_id"
     * @param string[] $usersItemsColumnArray e.g. ["comments"]
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    protected function universalFindItemsByParentId($id, $itemsTableName, $parentChildTableName, $childColumnName, $parentColumnName, $usersItemsTableName, $usersJoinColumnName, $usersItemsColumnArray)
    {
        $sql       = new Sql($this->adapter);
        $select    = $sql->select();
        $select->from($parentChildTableName);
        $select->columns([]);
        $select->join(
            $itemsTableName,
            "$childColumnName = $itemsTableName.id",
            $select::SQL_STAR,
            $select::JOIN_LEFT
        );
        $whereArray = ["$parentChildTableName.$parentColumnName = $id"];
        
        if (isset($this->user)) {
            $userId = $this->user->getId();
            $select->join(
                $usersItemsTableName,
                "$itemsTableName.id = $usersItemsTableName.$usersJoinColumnName",
                $usersItemsColumnArray,    
                $select::JOIN_LEFT
            );
            $whereArray[] = "($usersItemsTableName.user_id = $userId OR $usersItemsTableName.user_id IS NULL)";
        }
        $select->where($whereArray);                
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        if (! $result instanceof ResultInterface || ! $result->isQueryResult()) {
            return [];
        }

        $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
        $resultSet->initialize($result);
        $resultSet->buffer();
        return $resultSet;        
    }
            
            
    /**
     * @param int $id Id of the parent item
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    abstract public function findItemsByParentId($id);
    
    /**
     * @param SyllabusItemInterface $item
     * @param SyllabusItemInterface $parent If no parent, give $item twice
     */
    abstract public function insertItem(SyllabusItemInterface $item, SyllabusItemInterface $parent);

    /**
     * Method called by insertItem. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters. Delegates work to another two methods.
     * @param string $itemsTableName e.g. 'categories'
     * @param array $insertItemArray e.g. ['title' => $item->getTitle()]
     * @param string $objectClassName e.g. 'category'
     * @param string $usersItemsTableName e.g. 'users_categories'
     * @param array $insertUsersItemsArray e.g. ['title' => $item->getTitle()]
     * @param string $itemIdColumn e.g. 'category_id'
     * @param string $parentChildTableName e.g. 'syllabuses_categories'
     * @param array $insertParentChildArray e.g. ['category_id' => 1]
     * @return int Id of the new item
     */
    protected function universalInsertItem(
        $itemsTableName,
        $insertItemArray,
        $objectClassName,
        $usersItemsTableName,
        $insertUsersItemsArray,
        $itemIdColumn,
        $parentChildTableName,
        $insertParentChildArray
    )
    {
        $newId = $this->universalInsertIntoItems(
            $itemsTableName,
            $insertItemArray,
            $objectClassName
        );
        
        if ($newId) {
            if (!empty($parentChildTableName)) {
                $insertParentChildArray[$itemIdColumn] = $newId;
                $parentChildNewId = $this->universalInsertIntoParentChild(
                    $parentChildTableName,
                    $insertParentChildArray,
                    $objectClassName
                );
            } else {
                $parentChildNewId = -1;
            }

            $insertUsersItemsArray[$itemIdColumn] = $newId;
            $usersItemsNewId = $this->universalInsertIntoUsersItems(
                $usersItemsTableName,
                $insertUsersItemsArray,
                $objectClassName
            );
        }
        
        if ($parentChildNewId && $usersItemsNewId) {
            return $newId;
        }
        
        return null;
    }

    /**
     * Method called by universalInsertItem. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters. Inserts into the syllabusitems table, e.g. 'categories', 'topics'.
     * @param string $itemsTableName e.g. 'categories'
     * @param array $insertItemArray e.g. ['title' => $item->getTitle()]
     * @param string $objectClassName e.g. 'category'
     * @return int Id of the new item
     */
    protected function universalInsertIntoItems(
        $itemsTableName,
        $insertItemArray,
        $objectClassName    
    )
    {
        $insert = new Insert($itemsTableName);
        $insert->values($insertItemArray);

        $sql = new Sql($this->adapter);
        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();

        if (! $result instanceof ResultInterface) {
            throw new RuntimeException(
                "Database error occurred during $objectClassName insert operation"
            );
        }
        return $result->getGeneratedValue();
    }

    /**
     * Method called by universalInsertItem. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters. Inserts into the users_* table, e.g. 'users_categories'.
     * @param string $usersItemsTableName e.g. 'users_categories'
     * @param array $insertUsersItemsArray e.g. ['comments' => $item->getUserComments]
     * @param string $objectClassName e.g. 'category'
     * @return int Id of the new item
     */    
    protected function universalInsertIntoUsersItems(
        $usersItemsTableName,
        $insertUsersItemsArray,
        $objectClassName
    )
    {
        $insert = new Insert($usersItemsTableName);
        $insert->values($insertUsersItemsArray);

        $sql = new Sql($this->adapter);
        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();

        if (! $result instanceof ResultInterface) {
            throw new RuntimeException(
                "Database error occurred during $objectClassName insert operation"
            );
        }
        return $result->getGeneratedValue();
    }

    /**
     * Method called by universalInsertItem. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters. Inserts into the parent_child table, e.g. 'syllabuses_categories'.
     * @param string $parentChildTableName e.g. 'syllabuses_categories'
     * @param array $insertParentChildArray e.g. ['category_id' => 1]
     * @param string $objectClassName e.g. 'category'
     * @return int Id of the new item
     */    
    protected function universalInsertIntoParentChild(
        $parentChildTableName,
        $insertParentChildArray,
        $objectClassName
    )
    {
        $insert = new Insert($parentChildTableName);
        $insert->values($insertParentChildArray);

        $sql = new Sql($this->adapter);
        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();

        if (! $result instanceof ResultInterface) {
            throw new RuntimeException(
                "Database error occurred during $objectClassName insert operation"
            );
        }
        return $result->getGeneratedValue();
    }
    
    /**
     * Method called by updateItem. Implementation common to all SyllabusItemRepositoryInterface classes, but depending on the parameters
     * @param SyllabusItemInterface $item
     * @param string $itemsTableName e.g. "categories"
     * @param string $usersItemsTableName e.g. "users_categories"
     * @param string $usersJoinColumnName e.g. "category_id"
     * @param string $objectClassName e.g. "category"
     * @param array $updateItemsArray e.g. ['title' => $item->getTitle()]
     * @param array $updateUsersItemsArray - see above, but for users_* table
     * @return SyllabusItemInterface
     */
    protected function universalUpdateItem(
        SyllabusItemInterface $item,
        $itemsTableName,
        $usersItemsTableName,
        $usersJoinColumnName,
        $objectClassName,
        $updateItemsArray,
        $updateUsersItemsArray    
    )
    {
        if (! $item->getId()) {
            throw new RuntimeException("Cannot update $objectClassName; missing identifier");
        }

        $update = new Update($itemsTableName);
        $update->set($updateItemsArray);
        $update->where(['id = ?' => $item->getId()]);

        $sql = new Sql($this->adapter);
        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        if (! $result instanceof ResultInterface) {
            throw new RuntimeException(
                "Database error occurred during $objectClassName update operation"
            );
        }
        //now update of user-specific data
        $update2 = new Update($usersItemsTableName);
        $update2->set($updateUsersItemsArray);
        $update2->where([
            "$usersJoinColumnName = ?" => $item->getId(),
            'user_id = ?' => $this->user->getId()
        ]);
        $statement2 = $sql->prepareStatementForSqlObject($update2);
        $result2 = $statement2->execute();

        if (! $result2 instanceof ResultInterface) {
            throw new RuntimeException(
                "Database error occurred during $objectClassName update operation"
            );
        }
        return $item;
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    abstract public function updateItem(SyllabusItemInterface $item);
    
    /**
     * @param SyllabusItemInterface $item
     */
    abstract public function deleteItem(SyllabusItemInterface $item);
    
    /**
     * Each repository can have one child repository
     * @param SyllabusItemRepositoryInterface $repository
     */
    public function setChild(SyllabusItemRepositoryInterface $repository)
    {
        $this->childRepository = $repository;
    }
    
    /**
     * Returns direct child repository, which can be only one
     * @return SyllabusItemRepositoryInterface
     */
    public function getDirectChild()
    {
        return $this->childRepository;
    }
    
    /**
     * Returns this repository, its child, grand-child etc, the first that handles objects with the given class name
     * @param string $itemClass SyllabusItem class name, e.g. "Category"
     * @return SyllabusItemRepositoryInterface|null
     */
    public function getRepositoryByClass($itemClass)
    {
        if ($this->getItemClass() == $itemClass) {
            return $this;
        }
        if ($this->getDirectChild()) {
            return $this->getDirectChild()->getRepositoryByClass($itemClass);
        }
        return null;
    }
    
    /**
     * Returns the class name of the objects it handles
     * @return string
     */
    public function getItemClass()
    {
        return $this->objectPrototype->getClassName();
    }
    
    /**
     * Returns a clone of the prototype kept by the repository
     * @return SyllabusItemInterface
     */
    public function createItem()
    {
        if (!$this->user) {
            return null;
        }
        $newItem = clone $this->objectPrototype;
        $newItem->exchangeArray(['user_id' => $this->user->getId()]);
        return $newItem;
    }
    
    /**
     * Sets the user that should be associated with all the items handled by the repository. Each SyllabusItem object is related to two DB columns called "items" and "users_items". The former are data common to all users, the latter depend on the user that accesses the item. SyllabusItemRepository methods change their behaviour if the user is set. The user will be set for all child repositories.
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        if ($this->getDirectChild()) {
            $this->getDirectChild()->setUser($user);
        }
    }
    
}