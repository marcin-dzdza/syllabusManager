<?php
namespace Syllabus\Model;

use Zend\Db\ResultSet\ResultSetInterface;

/**
 * Interface for all structural elements of a syllabus: e.g. Syllabus, Category, Topic, Task
 */
interface SyllabusItemInterface
{
    /**
     * @return int
     */
    public function getId();
    /**
     * For populating the class with data from DB
     * @param array $data This will be provided by hydrator when extracting data from DB, array keys should match DB column names exactly
     */
    public function exchangeArray(array $data);
    /**
     * @param SyllabusItemInterface
     */
    public function addChild(SyllabusItemInterface $item);
    /**
     * @param ResultSetInterface $items Result from DB, a set of objects implementing SyllabusItemInterface
     */
    public function addChildren(ResultSetInterface $items);
    /**
     * @return SyllabusItemInterface[]|array Empty array if no children
     */
    public function getAllChildren();
    /**
     * @return InputFilterInterface Filter to be applied before using exchangeArray
     */
    public function getInputFilter();
    /**
     * @return string Name of the class
     */
    public function getClassName();

}