<?php
namespace Syllabus\Model;

/**
 * Represents one category. Corresponds to one row from DB table "categories" and "users_categories". Its elements are nested in $children.
 */
class Category extends SyllabusItem
{

    public function __construct($title, $description, $userId, $userComments, $id = -1)
    {
        parent::__construct($title, $description, $userId, $userComments, $id);
    }
    
    /**
     * For populating the class with data from DB
     * @param array $data This will be provided by hydrator when extracting data from DB, array keys match DB column names
     */
    public function exchangeArray(array $data)
    {
        parent::exchangeArray($data);
     }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }
        
        $this->inputFilter = parent::getInputFilter();      
              
        return $this->inputFilter;
    }

}