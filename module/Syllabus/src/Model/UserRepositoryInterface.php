<?php
namespace Syllabus\Model;

interface UserRepositoryInterface
{

    public function findAllUsers();

    public function findUserById($id);
    
    public function insertUser(User $user);
    
    public function updateUser(User $user);
    
    public function deleteUser(User $user);
     
}