<?php
namespace Syllabus\Model;

use Syllabus\Model\SyllabusItemRepository;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\HydratorInterface;

/**
 * For communication between class Topic and DB
 */
class TopicRepository extends SyllabusItemRepository
{
    /**
     * @var Topic
     */
    protected $objectPrototype;
    
    public function __construct(
        AdapterInterface $adapter,
        HydratorInterface $hydrator,
        Topic $objectPrototype
    )
    {
        parent::__construct($adapter, $hydrator, $objectPrototype);
    }

    /**
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findAllItems()
    {
        
    }

    /**
     * @param int $id Id of the item
     * @return SyllabusItemInterface
     */
    public function findItemById($id)
    {
        return $this->universalFindItemById(
            $id,
            'topics',
            'users_topics',
            'topic_id',
            [
                'comments'
            ],
            'topic'
        );
    }
    
    /**
     * @param int $id Id of the parent item
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findItemsByParentId($id)
    {
        return $this->universalFindItemsByParentId(
            $id,
            'topics',
            'categories_topics',
            'topic_id',
            'category_id',
            'users_topics',
            'topic_id',
            [
                'comments'
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     * @param SyllabusItemInterface $parent If no parent, give $item twice
     */
    public function insertItem(SyllabusItemInterface $item, SyllabusItemInterface $parent)
    {     
        $newId = $this->universalInsertItem(
            'topics',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId(),
                'rank' => $item->getRank()
            ],
            'topic',
            'users_topics',
            [
                'user_id' => $item->getUserId(),
                'comments' => $item->getUserComments()
            ],
            'topic_id',
            'categories_topics',
            [
                'category_id' => $parent->getId()
            ]
        );
        
        return new Topic(
            $item->getTitle(),
            $item->getDescription(),
            $item->getUserId(),
            $item->getUserComments(),
            $item->getRank(),
            $newId
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function updateItem(SyllabusItemInterface $item)
    {        
        return $this->universalUpdateItem(
            $item,
            'topics',
            'users_topics',
            'topic_id',
            'topic',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'rank' => $item->getRank(),
                'user_id' => $item->getUserId()
            ],
            [
                'comments' => $item->getUserComments()
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function deleteItem(SyllabusItemInterface $item)
    {
        
    }
    
}