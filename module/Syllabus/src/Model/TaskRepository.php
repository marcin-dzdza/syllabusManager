<?php
namespace Syllabus\Model;

use Syllabus\Model\SyllabusItemRepository;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\HydratorInterface;

/**
 * For communication between class Task and DB
 */
class TaskRepository extends SyllabusItemRepository
{
    /**
     * @var Task
     */
    protected $objectPrototype;
    
    public function __construct(
        AdapterInterface $adapter,
        HydratorInterface $hydrator,
        Task $objectPrototype
    )
    {
        parent::__construct($adapter, $hydrator, $objectPrototype);
    }

    /**
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findAllItems()
    {
        
    }

    /**
     * @param int $id Id of the item
     * @return SyllabusItemInterface
     */
    public function findItemById($id)
    {
        return $this->universalFindItemById(
            $id,
            'tasks',
            'users_tasks',
            'task_id',
            [
                'comments'
            ],
            'task'
        );
    }
    
    /**
     * @param int $id Id of the parent item
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findItemsByParentId($id)
    {
        return $this->universalFindItemsByParentId(
            $id,
            'tasks',
            'topics_tasks',
            'task_id',
            'topic_id',
            'users_tasks',
            'task_id',
            [
                'comments'
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     * @param SyllabusItemInterface $parent If no parent, give $item twice
     */
    public function insertItem(SyllabusItemInterface $item, SyllabusItemInterface $parent)
    {     
        $newId = $this->universalInsertItem(
            'tasks',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId()
            ],
            'task',
            'users_tasks',
            [
                'user_id' => $item->getUserId(),
                'comments' => $item->getUserComments()
            ],
            'task_id',
            'topics_tasks',
            [
                'topic_id' => $parent->getId()
            ]
        );
        
        return new Task(
            $item->getTitle(),
            $item->getDescription(),
            $item->getUserId(),
            $item->getUserComments(),
            $newId
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function updateItem(SyllabusItemInterface $item)
    {        
        return $this->universalUpdateItem(
            $item,
            'tasks',
            'users_tasks',
            'task_id',
            'task',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId()
            ],
            [
                'comments' => $item->getUserComments()
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function deleteItem(SyllabusItemInterface $item)
    {
        
    }
    
}