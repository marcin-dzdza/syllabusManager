<?php
namespace Syllabus\Model;

// walidacja
use Zend\Filter\ToInt;

/**
 * Represents one syllabus. Corresponds to one row from DB table "syllabuses" and "users_syllabuses". Its elements are nested in $children.
 */
class Syllabus extends SyllabusItem
{
    /**
     * @var int Who can access this syllabus. -1 = only the owner, 0 = everyone
     */
    protected $access;

    public function __construct($title, $description, $userId, $userComments, $access, $id = -1)
    {
        parent::__construct($title, $description, $userId, $userComments, $id);
        $this->access = $access;
    }
    
    /**
     * For populating the class with data from DB
     * @param array $data This will be provided by hydrator when extracting data from DB, array keys match DB column names
     */
    public function exchangeArray(array $data)
    {
        parent::exchangeArray($data);
        $this->access     = isset($data['access']) ? $data['access'] : $this->access;
     }

    /**
     * 
     * @return int
     */
    function getAccess()
    {
        return $this->access;
    }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }
        
        $this->inputFilter = parent::getInputFilter();
       
        $this->inputFilter->add([
            'name' => 'access',
            'required' => false,
            'filters' => [
                [
                    'name' => ToInt::class
                ],
            ],
        ]);            
              
        return $this->inputFilter;
    }

}