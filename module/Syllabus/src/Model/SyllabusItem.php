<?php
namespace Syllabus\Model;

use Zend\Db\ResultSet\ResultSetInterface;

//related to input filters
use DomainException;
use Zend\Filter\StringTrim;
use Zend\Filter\ToInt;
use Zend\Filter\Whitelist;
use Zend\Validator\GreaterThan;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;

/**
 * Parent of all structural elements of a syllabus: e.g. Syllabus, Category, Topic, Task
 */
abstract class SyllabusItem implements SyllabusItemInterface
{
    /**
     * @var int Corresponds to "id" in DB, otherwise -1 
     */
    protected $id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string 
     */
    protected $description;
    /**
     * @var int User who created it (and considered to be the owner)
     */
    protected $userId;
    /**
     * @var string|null User-specific text (from DB users_*, e.g. users_syllabuses). Depends on the user who uses the syllabus, not the user who created it
     */
    protected $userComments;
    /**
     *
     * @var SyllabusItemInterface[]|array Empty array if no children
     */
    protected $children;
    /**
     *
     * @var InputFilterInterface Filter to be applied to values before populating the object 
     */
    protected $inputFilter;    
    
    public function __construct($title, $description, $userId, $userComments, $id = -1)
    {
        $this->title = $title;
        $this->description = $description;
        $this->userId = $userId;
        $this->userComments = $userComments;
        $this->id = $id;
        $this->children = [];
    }    
    
    /**
     * For populating the class with data from DB
     * @param array $data This will be provided by hydrator when extracting data from DB, array keys should match DB column names exactly
     */
    public function exchangeArray(array $data)
    {
        
        $this->id     = isset($data['id']) ? $data['id'] : $this->id;
        $this->title     = isset($data['title']) ? $data['title'] : $this->title;
        $this->description     = isset($data['description']) ? $data['description'] : $this->description;
        $this->userId     = isset($data['user_id']) ? $data['user_id'] : $this->userId;
        $this->userComments     = isset($data['comments']) ? $data['comments'] : $this->userComments;
    }
    
    /**
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * 
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * 
     * @return string|null
     */
    function getUserComments()
    {
        return $this->userComments;
    }    

    /**
     * @param SyllabusItemInterface
     */
    public function addChild(SyllabusItemInterface $item)
    {
        $this->children[] = $item;
    }
    
    /**
     * @param ResultSetInterface $items Result from DB, a set of objects implementing SyllabusItemInterface
     */
    public function addChildren(ResultSetInterface $items)
    {
        foreach ($items as $item) {
            $this->addChild($item);
        }
    }
    
    /**
     * @return SyllabusItemInterface[]|array Empty array if no children
     */
    public function getAllChildren()
    {
        return $this->children;
    }
    
    /**
     * @return string Name of the class
     */    
    public function getClassName()
    {
        $classname = get_class($this);
        if ($pos = strrpos($classname, '\\')) {
            return substr($classname, $pos + 1);
        }
        return $pos;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    /**
     * @return InputFilterInterface Method not meant to be extended. Filter related to input that specifies which SyllabusItem object should be instantiated and modified. Data has the form: Table.id.column, e.g. 'Category.1.description'
     */    
    static public function getGeneralEditFilter()
    {
        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'table',
            'required' => true,
            'filters' => [
                [
                    'name' => Whitelist::class,
                    'options' => [
                        'list'=> ['Syllabus','Category','Topic','Task']
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'column',
            'required' => true,
            'filters' => [
                [
                    'name' => Whitelist::class,
                    'options' => [
                        'list'=> ['id','title','description','comments',
                            'userId', 'access']
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                [
                    'name' => ToInt::class
                ],
            ],
            'validators' => [
                [
                    'name' => GreaterThan::class,
                    'options' => [
                        'min'=> 1,
                        'inclusive' => true
                    ]
                ],                
            ],
        ]);
        
        return $inputFilter; 
    }
    
    /**
     * @return InputFilterInterface Filter to be applied before using exchangeArray
     */
    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        
        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                [
                    'name' => ToInt::class
                ],
            ],
            'validators' => [
                [
                    'name' => GreaterThan::class,
                    'options' => [
                        'min'=> 1,
                        'inclusive' => true
                    ]
                ],                
            ],
        ]);        
        
        $inputFilter->add([
            'name' => 'userId',
            'required' => false,
            'filters' => [
                [
                    'name' => ToInt::class
                ],
            ],
            'validators' => [
                [
                    'name' => GreaterThan::class,
                    'options' => [
                        'min'=> 1,
                        'inclusive' => true
                    ]
                ],                
            ],
        ]);

        $inputFilter->add([
            'name' => 'title',
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'description',
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'comments',
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);
            
        return $inputFilter;
    }
}