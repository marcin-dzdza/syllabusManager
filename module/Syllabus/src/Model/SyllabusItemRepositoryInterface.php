<?php
namespace Syllabus\Model;

/**
 * Interface for repositories of syllabuses, categories, topics etc.
 */
interface SyllabusItemRepositoryInterface
{

    /**
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findAllItems();

    /**
     * @param int $id Id of the item
     * @return SyllabusItemInterface
     */
    public function findItemById($id);
    
    /**
     * @param int $id Id of the parent item
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findItemsByParentId($id);
    
    /**
     * @param SyllabusItemInterface $item
     * @param SyllabusItemInterface $parent If no parent, give $item twice
     */
    public function insertItem(SyllabusItemInterface $item, SyllabusItemInterface $parent);
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function updateItem(SyllabusItemInterface $item);
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function deleteItem(SyllabusItemInterface $item);
    
    /**
     * Each repository can have one child repository
     * @param SyllabusItemRepositoryInterface $repository
     */
    public function setChild(SyllabusItemRepositoryInterface $repository);
    
    /**
     * Returns direct child repository, which can be only one
     * @return SyllabusItemRepositoryInterface|null
     */
    public function getDirectChild();
    
    /**
     * Returns this repository, its child, grand-child etc, the first that handles objects with the given class name
     * @param string $itemClass SyllabusItem class name, e.g. "Category"
     * @return SyllabusItemRepositoryInterface|null
     */
    public function getRepositoryByClass($itemClass);
    
    /**
     * Returns the class name of the objects it handles
     * @return string
     */
    public function getItemClass();

    /**
     * Returns a clone of the prototype kept by the repository
     * @return SyllabusItemInterface
     */
    public function createItem();    
    /**
     * Sets the user that should be associated with all the items handled by the repository. Each SyllabusItem object is related to two DB columns called "items" and "users_items". The former are data common to all users, the latter depend on the user that accesses the item. SyllabusItemRepository methods change their behaviour if the user is set. The user will be set for all child repositories.
     * @param User $user
     */
    public function setUser(User $user);
    
}