<?php
namespace Syllabus\Model;

// walidacja
use Zend\Filter\ToInt;

/**
 * Represents one topic. Corresponds to one row from DB table "topics" and "users_topics". Its elements are nested in $children.
 */
class Topic extends SyllabusItem
{
    /**
     * @var int How important is this topic
     */
    private $rank;

    public function __construct($title, $description, $userId, $userComments, $rank, $id = -1)
    {
        parent::__construct($title, $description, $userId, $userComments, $id);
        $this->rank = $rank;
    }
    
    /**
     * For populating the class with data from DB
     * @param array $data This will be provided by hydrator when extracting data from DB, array keys match DB column names
     */
    public function exchangeArray(array $data)
    {
        parent::exchangeArray($data);
        $this->rank     = isset($data['rank']) ? $data['rank'] : $this->rank;
     }

    /**
     * 
     * @return int
     */
    function getRank()
    {
        return $this->rank;
    }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }
        
        $this->inputFilter = parent::getInputFilter();      
       
        $this->inputFilter->add([
            'name' => 'rank',
            'required' => false,
            'filters' => [
                [
                    'name' => ToInt::class
                ],
            ],
        ]);            
              
        return $this->inputFilter;
    }

}