<?php
namespace Syllabus\Model;

/**
 * Represents one task. Corresponds to one row from DB table "tasks" and "users_tasks". Its elements are nested in $children.
 */
class Task extends SyllabusItem
{

    public function __construct($title, $description, $userId, $userComments, $id = -1)
    {
        parent::__construct($title, $description, $userId, $userComments, $id);
    }
    
    /**
     * For populating the class with data from DB
     * @param array $data This will be provided by hydrator when extracting data from DB, array keys match DB column names
     */
    public function exchangeArray(array $data)
    {
        parent::exchangeArray($data);
     }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }
        
        $this->inputFilter = parent::getInputFilter();      
              
        return $this->inputFilter;
    }

}