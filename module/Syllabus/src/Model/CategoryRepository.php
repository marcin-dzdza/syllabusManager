<?php
namespace Syllabus\Model;

use Syllabus\Model\SyllabusItemRepository;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\HydratorInterface;

/**
 * For communication between class Category and DB
 */
class CategoryRepository extends SyllabusItemRepository
{
    /**
     * @var Category
     */
    protected $objectPrototype;
    
    public function __construct(
        AdapterInterface $adapter,
        HydratorInterface $hydrator,
        Category $objectPrototype
    )
    {
        parent::__construct($adapter, $hydrator, $objectPrototype);
    }

    /**
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findAllItems()
    {
        
    }

    /**
     * @param int $id Id of the item
     * @return SyllabusItemInterface
     */
    public function findItemById($id)
    {
        return $this->universalFindItemById(
            $id,
            'categories',
            'users_categories',
            'category_id',
            [
                'comments'
            ],
            'category'
        );
    }
    
    /**
     * @param int $id Id of the parent item
     * @return ResultSetInterface|array A set of objects implementing SyllabusItemInterface or an empty array
     */
    public function findItemsByParentId($id)
    {
        return $this->universalFindItemsByParentId(
            $id,
            'categories',
            'syllabuses_categories',
            'category_id',
            'syllabus_id',
            'users_categories',
            'category_id',
            [
                'comments'
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     * @param SyllabusItemInterface $parent If no parent, give $item twice
     */
    public function insertItem(SyllabusItemInterface $item, SyllabusItemInterface $parent)
    {     
        $newId = $this->universalInsertItem(
            'categories',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId()
            ],
            'category',
            'users_categories',
            [
                'user_id' => $item->getUserId(),
                'comments' => $item->getUserComments()
            ],
            'category_id',
            'syllabuses_categories',
            [
                'syllabus_id' => $parent->getId()
            ]
        );
        
        return new Category(
            $item->getTitle(),
            $item->getDescription(),
            $item->getUserId(),
            $item->getUserComments(),
            $newId
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function updateItem(SyllabusItemInterface $item)
    {        
        return $this->universalUpdateItem(
            $item,
            'categories',
            'users_categories',
            'category_id',
            'category',
            [
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'user_id' => $item->getUserId()
            ],
            [
                'comments' => $item->getUserComments()
            ]
        );
    }
    
    /**
     * @param SyllabusItemInterface $item
     */
    public function deleteItem(SyllabusItemInterface $item)
    {
        
    }
    
}