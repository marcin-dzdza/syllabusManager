<?php
namespace Syllabus\Model;

use InvalidArgumentException;
use RuntimeException;
use Zend\Db\Adapter\AdapterInterface;
use Syllabus\Model\UserRepositoryInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Hydrator\HydratorInterface;
use Zend\Db\ResultSet\HydratingResultSet;

class SqlUserRepository implements UserRepositoryInterface
{
    private $adapter;
    private $hydrator;
    private $userPrototype;
    
    public function __construct(
        AdapterInterface $adapter,
        HydratorInterface $hydrator,
        User $userPrototype
    ) {
        $this->adapter = $adapter;
        $this->hydrator = $hydrator;
        $this->userPrototype = $userPrototype;
    }
    
    public function findAllUsers()
    {
        $sql       = new Sql($this->adapter);
        $select    = $sql->select('users');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        if (! $result instanceof ResultInterface || ! $result->isQueryResult()) {
            return [];
        }

        $resultSet = new HydratingResultSet($this->hydrator, $this->userPrototype);
        $resultSet->initialize($result);
        $resultSet->buffer();
        return $resultSet;
    }

    public function findUserById($id)
    {
        $sql       = new Sql($this->adapter);
        $select    = $sql->select('users');
        $select->where(['id = ?' => $id]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        if (! $result instanceof ResultInterface || ! $result->isQueryResult()) {
            throw new RuntimeException(sprintf(
                'Failed retrieving user with identifier "%s"; unknown database error.',
                $id
            ));
        }

        $resultSet = new HydratingResultSet($this->hydrator, $this->userPrototype);
        $resultSet->initialize($result);
        $user = $resultSet->current();

        if (! $user) {
            throw new InvalidArgumentException(sprintf(
                'User with identifier "%s" not found.',
                $id
            ));
        }

        return $user;
    }
    
    public function insertUser(User $user)
    {
        
    }
    
    public function updateUser(User $user)
    {
        
    }
    
    public function deleteUser(User $user)
    {
        
    }
    
}