<?php
namespace Syllabus;

use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'aliases' => [
            Model\SyllabusItemRepositoryInterface::class => Model\SyllabusRepository::class,
            Model\UserRepositoryInterface::class => Model\SqlUserRepository::class,
        ],
        'factories' => [
            Model\SyllabusRepository::class => Factory\SyllabusRepositoryFactory::class,
            Model\SqlUserRepository::class => Factory\SqlUserRepositoryFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\SyllabusController::class => Factory\SyllabusControllerFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'syllabus' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/syllabus[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\SyllabusController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'syllabus' => __DIR__ . '/../view',
        ],
    ],
];